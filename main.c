/*
 * Copyright 2020 Nico Sonack
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *
 * 1. Redistributions of source code must retain the above copyright
 * notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above
 * copyright notice, this list of conditions and the following
 * disclaimer in the documentation and/or other materials provided
 * with the distribution.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 * INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <fcntl.h>
#include <inttypes.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <unistd.h>

#include <pthread.h>

#include <libswresample/swresample.h>
#include <libavutil/opt.h>

#include <portaudio.h>

#include <rtl-sdr.h>

#define GAIN 1.0
#define SAMPLE_RATE 256000
#define BANDWIDTH 200000

struct buffer {
    uint8_t *data;
    size_t   length;
};

#define Q_MAX_SIZE 64

struct queue {
    struct buffer buffer[Q_MAX_SIZE];
    size_t begin, size;
    pthread_mutex_t q_lock;
    pthread_cond_t  q_sig;
};

static void
queue_init(struct queue *q)
{
    if ( pthread_mutex_init(&q->q_lock, NULL) != 0 ) {
        fprintf(stderr, "ERR : Unable to init mutex\n");
        abort();
    }

    if ( pthread_cond_init(&q->q_sig, NULL) != 0 ) {
        fprintf(stderr, "ERR : Unable to init condition variable\n");
        abort();
    }
}

static void
queue_nq(struct queue *q, struct buffer elem)
{
    if ( pthread_mutex_lock(&q->q_lock) != 0 ) {
        fprintf(stderr, "ERR : Unable to lock queue\n");
        abort();
    }

    q->buffer[(q->begin + q->size) % Q_MAX_SIZE] = elem;

    if ( q->size < Q_MAX_SIZE )
        q->size += 1;
    else
        q->begin = (q->begin + 1) % Q_MAX_SIZE;

    if ( pthread_cond_signal(&q->q_sig) != 0 ) {
        fprintf(stderr, "ERR : Unable to signal queue\n");
        abort();
    }

    if ( pthread_mutex_unlock(&q->q_lock) != 0 ) {
        fprintf(stderr, "ERR : Unable to unlock queue\n");
        abort();
    }
}

static struct buffer
queue_dq(struct queue *q)
{
    while ( !q->size ) {
        if ( pthread_cond_wait(&q->q_sig, &q->q_lock) != 0 ) {
            fprintf(stderr, "ERR : Unable to wait for queue signal\n");
            abort();
        }
    }

    struct buffer elem = q->buffer[q->begin];
    q->begin = (q->begin + 1) % Q_MAX_SIZE;
    q->size -= 1;

    return elem;
}

static double
clamp(double x, double min, double max)
{
    if ( x < min ) {
        return min;
    } else if ( x > max ) {
        return max;
    } else {
        return x;
    }
}

static void
cb(unsigned char *buf, uint32_t len, void *ctx)
{
    struct queue *q = (struct queue *)(ctx);

    /*
     * Copy the buffer, as it might get reused internally by librtlsdr
     */
    uint8_t *cpy = calloc(1, len);
    memcpy(cpy, buf, len);

    queue_nq(q, (struct buffer) { .data = cpy, .length = len });
}

static void
recv_loop(struct queue *q)
{
    /* Gotta love ffmpeg /sarc */
    struct SwrContext *swr = swr_alloc();
    av_opt_set_int(swr, "in_channel_count",  1, 0);
    av_opt_set_int(swr, "out_channel_count", 1, 0);
    av_opt_set_int(swr, "in_channel_layout",  AV_CH_LAYOUT_MONO, 0);
    av_opt_set_int(swr, "out_channel_layout", AV_CH_LAYOUT_MONO, 0);
    av_opt_set_int(swr, "in_sample_rate", SAMPLE_RATE, 0);
    av_opt_set_int(swr, "out_sample_rate", 44100, 0);
    av_opt_set_sample_fmt(swr, "in_sample_fmt",  AV_SAMPLE_FMT_DBL, 0);
    av_opt_set_sample_fmt(swr, "out_sample_fmt", AV_SAMPLE_FMT_S16,  0);
    swr_init(swr);
    if ( !swr_is_initialized(swr) ) {
        fprintf(stderr, "Unable to initizialize resampler.\n");
        abort();
    }

    PaError   error;
    PaStream *stream = NULL;

    error = Pa_Initialize();
    if ( error != paNoError ) {
        fprintf(stderr, "ERR : Unable to init Portaudio\n");
        abort();
    }

    error = Pa_OpenDefaultStream(&stream, 0, 1, paInt16, 44100, 0, NULL, NULL);
    if ( error != paNoError ) {
        fprintf(stderr, "ERR : Unable to open stream with Portaudio\n");
        abort();
    }

    error = Pa_StartStream(stream);
    if ( error != paNoError ) {
        fprintf(stderr, "ERR : Unable to start stream with Portaudio\n");
        abort();
    }

    if ( pthread_mutex_lock(&q->q_lock) != 0) {
        fprintf(stderr, "ERR : Unable to lock queue\n");
        abort();
    }

    for (;;) {
        struct buffer  buf    = queue_dq(q);
        size_t         length = buf.length;
        uint8_t       *data   = buf.data;
        size_t         n      = length / 2;

        double *things1 = malloc(sizeof(double) * 2 * (n - 1));
        if ( !things1 ) {
            fprintf(stderr, "Unable to malloc. Again\n");
            abort();
        }

        for ( size_t i = 0; i < n - 1; ++i ) {
            /* z1 = a + bi, z0 = c + di */
            double a, b, c, d;
#define center(x) (((double)x - 128.0) / 128.0)
            a =  center(data[2*i + 0]);
            b =  center(data[2*i + 1]);
            c =  center(data[2*i + 2]);
            d =  center(data[2*i + 3]);
#undef center

            things1[2*i + 0] = ( a * c + b * d );
            things1[2*i + 1] = ( b * c - a * d );
        }

        double *dthetas = malloc(sizeof(double) * (n - 1));
        if ( !dthetas ) {
            fprintf(stderr, "Unable to malloc. Yet again.\n");
            abort();
        }

        double magic = GAIN * (double)(SAMPLE_RATE) / (BANDWIDTH * 2 * M_PI);
        for ( size_t i = 0; i < n - 1; ++i ) {
            double x, y, r;
            x = things1[2*i];
            y = things1[2*i+1];
            r = sqrt(x*x+y*y);

            double rotation = atan2(y, x);
            rotation *= r / fabs(r);
            rotation = fmod(rotation + M_PI, 2 * M_PI) - M_PI;
            dthetas[i] = clamp(rotation * magic, -1.0, 1.0);
        }

        uint8_t* resampled_data = malloc(sizeof(double) * (n-1));
        int n_converted_samples = swr_convert(swr, &resampled_data, (n-1), (const uint8_t**)&dthetas, (n-1));
        if ( n_converted_samples < 0 ) {
            fprintf(stderr, "Unable to resample demodulated data\n");
            abort();
        }

        error = Pa_WriteStream(stream, resampled_data, n_converted_samples);
        if ( error != paNoError ) {
            fprintf(stderr, "Unable to push data to portaudio\n");
            abort();
        }

        free(data);
        free(resampled_data);
        free(dthetas);
        free(things1);
    }
}

void *
run_recv_loop(void *ctx)
{
    recv_loop((struct queue *)(ctx));
    return NULL;
}

int
main(void)
{
    uint32_t devices_n = rtlsdr_get_device_count();

    if (devices_n < 1) {
        fprintf(stderr, "ERR : No devices available\n");
        return EXIT_FAILURE;
    }

    rtlsdr_dev_t *device = NULL;
    if (rtlsdr_open(&device, 0) < 0) {
        fprintf(stderr, "ERR : Unable to open default device\n");
        return EXIT_FAILURE;
    }

    rtlsdr_set_tuner_bandwidth(device, BANDWIDTH);

    rtlsdr_set_sample_rate(device, SAMPLE_RATE);
    uint32_t sample_rate = rtlsdr_get_sample_rate(device);

    rtlsdr_set_tuner_gain(device, 340);

    printf("INFO : Device sample rate is %u Hz\n", sample_rate);

    struct queue q = {0};
    queue_init(&q);

    rtlsdr_reset_buffer(device);
    rtlsdr_set_center_freq(device, 93600000);

    pthread_t recv_thread;
    pthread_create(&recv_thread, NULL, run_recv_loop, &q);

    rtlsdr_read_async(device, cb, &q, 0, 0);

    rtlsdr_close(device);

    return EXIT_SUCCESS;
}
