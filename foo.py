#
# WARNING: THIS IS NOT MY CODE. I WOULD NEVER WRITE PYTHON.
# STOLEN FROM: https://github.com/nuffleee
#

import numpy as np
from pylab import *
import numpy as np
import struct

with open('teste.iq', 'rb') as f:
  data = np.fromfile(f, dtype='uint8')

  data = data - 127.5
  data = data / 128.0

  print(data[0], data[1])
  print(complex(data[0], data[1]))

  data = data.view(complex)

  sample_rate = 256000

  data = data[1:] * np.conj(data[:-1])
  output_raw = np.arctan2(np.imag(data), np.real(data))

  # Clip
  output_raw = np.clip(output_raw, -0.999, +0.999)

  # Scale to signed 16-bit int
  output_raw = np.multiply(output_raw, 32767)
  output_raw = output_raw.astype(int)

  # Output as raw 16-bit, 1 channel audio
  bits = struct.pack(('<%dh' % len(output_raw)), *output_raw)

  with open('audio.raw', 'wb') as f:
    f.write(bits)
