import std.stdio;
import std.complex;
import std.file;
import std.range;
import std.algorithm;
import std.array;
import std.math;

void main(string[] args) {
    if (args.length != 3) {
        stderr.writeln("usage: sdr <input> <output>.");
        return;
    }

    auto data = cast(ubyte[])(std.file.read(args[1]));

    ulong n = data.length / 2;

    Complex!double[] thetas = new Complex!double[n];
    for (uint i = 0; i < n; ++i) {
        thetas[i] = complex((data[2*i]-127.0) / 128.0, (data[2*i+1]-127.0) / 128.0);
    }

    auto dthetas = std.range.zip(
        thetas[1..thetas.length-1],
        thetas[0..thetas.length-2])
        .map!(x => arg(x[0] * conj(x[1])))
        .map!(x => x * M_1_PI);
    std.file.write(args[2], array(dthetas.map!(x => cast(short)(x * 32676.0))));
}
