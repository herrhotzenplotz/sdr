CC=	/usr/bin/cc
PKGS=	libswresample libavutil portaudio-2.0 librtlsdr
LDFLAGS=`pkg-config --libs $(PKGS)` -lm -lpthread -lrt
CFLAGS=	-std=c99 -g -pedantic -Wall -Wextra `pkg-config --cflags $(PKGS)`
PROG=	snfmrad
SRCS=	main.c
MK_MAN=	no

.include <bsd.prog.mk>
